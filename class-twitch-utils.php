<?php
class Twitch_Utils {
	public function init() {
	}

	public static function get_twitch_access_token(): string {
		$access_token = get_transient( 'twitch-is-live-check-token' );
		if ( ! $access_token ) {
			$token_response      = wp_remote_post(
				'https://id.twitch.tv/oauth2/token?client_id=' . TWITCH_CLIENT_ID . '&client_secret=' . TWITCH_CLIENT_SECRET . '&grant_type=client_credentials',
				array(
					'timeout' => 15,
				)
			);
			$token_response_body = json_decode( $token_response['body'] );
			$access_token        = $token_response_body->access_token;
			$expires_in          = $token_response_body->expires_in;
			set_transient( 'twitch-is-live-check-token', sanitize_key( $access_token ), intval( $expires_in ) );
		}
		return $access_token;
	}

	public static function get_twitch_live_background( int $author_id ): string {
		$live_background = get_transient( 'twitch-' . $author_id . '-live-background' );
		if ( ! $live_background ) {
			$twitch_username  = get_the_author_meta( TWITCH_USER_FIELD, (int) $author_id );
			$live_backgrounds = get_transient( 'twitch-live-backgrounds' );
			if ( $live_backgrounds && array_key_exists( $twitch_username, $live_backgrounds ) ) {
				$live_background = $live_backgrounds[ $twitch_username ];
			} else {
				$live_background       = 'offline';
				$access_token          = self::get_twitch_access_token();
				$streams_response      = wp_remote_get(
					'https://api.twitch.tv/helix/streams?user_login=' . $twitch_username,
					array(
						'timeout' => 15,
						'headers' => array(
							'Client-ID'     => TWITCH_CLIENT_ID,
							'Authorization' => 'Bearer ' . $access_token,
						),
					)
				);
				$streams_response_body = json_decode( $streams_response['body'] );
				if ( $streams_response_body ) {
					if ( $streams_response_body->data && count( $streams_response_body->data ) > 0 ) {
						$live_background = $streams_response_body->data[0]->thumbnail_url;
					}
					set_transient( 'twitch-' . $author_id . '-live-background', $live_background, intval( MINUTE_IN_SECONDS ) );
				}
			}
		}
		return $live_background;
	}

	public static function get_twitch_live_backgrounds( array $twitch_usernames, int $limit = 10 ): array {
		$live_backgrounds = get_transient( 'twitch-live-backgrounds' );
		if ( ! $live_backgrounds ) {
			$live_backgrounds     = array();
			$access_token         = self::get_twitch_access_token();
			$twitch_usernames_str = implode( '&user_login=', $twitch_usernames );

			$streams_response      = wp_remote_get(
				'https://api.twitch.tv/helix/streams?limit=' . absint( $limit ) . 'user_login=' . $twitch_usernames_str,
				array(
					'timeout' => 15,
					'headers' => array(
						'Client-ID'     => TWITCH_CLIENT_ID,
						'Authorization' => 'Bearer ' . $access_token,
					),
				)
			);
			$streams_response_body = json_decode( $streams_response['body'] );
			if ( $streams_response_body && $streams_response_body->data ) {
				foreach ( $streams_response_body->data as $stream_data ) {
					$live_backgrounds[ $stream_data->user_login ] = $stream_data->thumbnail_url;
				}
				set_transient( 'twitch-live-backgrounds', $live_backgrounds, intval( MINUTE_IN_SECONDS ) );
			}
		}
		return $live_backgrounds;
	}

	public static function get_top_100_streamers(): array {
		$top_creators_ids = Follow_Author::get_authors_with_more_followers( 200 );
		$twitch_usernames = array();
		foreach ( $top_creators_ids as $creator_id ) {
			if ( count( $twitch_usernames ) >= 100 ) {
				return $twitch_usernames;
			}
			$twitch_username = get_the_author_meta( TWITCH_USER_FIELD, (int) $creator_id );
			if ( $twitch_username ) {
				$twitch_usernames[] = $twitch_username;
			}
		}

		return $twitch_usernames;
	}

	public static function get_top_live_streamers( int $limit = 10 ) {
		$top_streamers = self::get_top_100_streamers();
		return self::get_twitch_live_backgrounds( $top_streamers, $limit );
	}

	public static function set_background_size( string $background_url, int $height = 315, int $width = 560 ): string {
		return str_replace( '{height}', $height, str_replace( '{width}', $width, $background_url ) );
	}

	public static function get_author_id_from_twitch_username( string $twitch_username ): int {
		$args  = array(
			'role'       => 'author',
			'meta_key'   => TWITCH_USER_FIELD,
			'meta_value' => $twitch_username,
		);
		$users = get_users( $args );

		if ( count( $users ) > 0 ) {
			return $users[0]->ID;
		}

		return 0;
	}
}

$twitch_utils = new Twitch_Utils();
add_action( 'init', array( $twitch_utils, 'init' ) );
