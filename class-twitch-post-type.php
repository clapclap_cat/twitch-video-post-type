<?php
/**
 * Plugin Name: Twitch video post type
 */

define( 'TWITCH_SLUG', 'twitch' );
define( 'TWITCH_BRAND_NAME', 'Twitch' );
define( 'TWITCH_USER_FIELD', 'twitch-username' );
define( 'TWITCH_VIDEO_POST_TYPE', 'twitch-video' );
define( 'TWITCH_VIDEO_POST_TYPE_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) );
define( 'TWITCH_VIDEO_POST_TYPE_PATH', plugin_dir_path( __FILE__ ) );

require TWITCH_VIDEO_POST_TYPE_PATH . 'class-twitch-utils.php';

class Twitch_Post_Type {
	public function init() {
		if ( is_admin() ) {
			load_plugin_textdomain( 'twitch-video-post-type', false, dirname( plugin_basename( __FILE__ ) ) );
			$twitch_username = get_the_author_meta( TWITCH_USER_FIELD, get_current_user_id() );
			if ( current_user_can( 'administrator' ) || $twitch_username ) {
				wp_register_style( 'twitch-video-admin-style', TWITCH_VIDEO_POST_TYPE_URL . '/style-admin.css', array(), filemtime( TWITCH_VIDEO_POST_TYPE_PATH . '/style-admin.css' ) );
				wp_enqueue_style( 'twitch-video-admin-style' );
				add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ) );
			} else {
				add_filter( 'admin_head', array( __CLASS__, 'hide_admin_menu_item' ) );
			}

			add_filter( 'gutenberg_can_edit_post_type', array( __CLASS__, 'gutenberg_can_edit_post_type' ), 10, 2 );
			add_filter( 'use_block_editor_for_post_type', array( __CLASS__, 'gutenberg_can_edit_post_type' ), 10, 2 );

			add_action( 'show_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 8 );
			add_action( 'edit_user_profile', array( __CLASS__, 'extra_user_profile_fields' ), 8 );
			add_action( 'personal_options_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
			add_action( 'edit_user_profile_update', array( __CLASS__, 'save_extra_user_profile_fields' ) );
			add_action( 'user_profile_update_errors', array( __CLASS__, 'validate_extra_user_profile_fields' ), 10, 3 );
		} else {
			add_filter(
				'clapclap_author_social_icons',
				function( $social_icons, $author_id ) {
					$twitch_username = get_the_author_meta( TWITCH_USER_FIELD, (int) $author_id );
					if ( $twitch_username ) {
						$social_icons[] = array(
							'slug' => TWITCH_SLUG,
							'name' => TWITCH_BRAND_NAME,
							'icon' => TWITCH_VIDEO_POST_TYPE_URL . '/icons/brand-twitch.svg',
							'link' => 'https://www.twitch.tv/' . $twitch_username . '/',
						);
					}
					return $social_icons;
				},
				10,
				2
			);
		}
		register_post_type(
			TWITCH_VIDEO_POST_TYPE,
			array(
				'label'              => __( 'Twitch streams', 'twitch-video-post-type' ),
				'public'             => true,
				'show_in_rest'       => true,
				'publicly_queryable' => true,
				'supports'           => array( 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'title', 'editor' ),
				'has_archive'        => true,
				'taxonomies'         => array( 'post_tag', 'category' ),
				'rewrite'            => array(
					'slug' => 'u/%author%/' . TWITCH_SLUG,
				),
				'capabilities'       => array(
					'create_posts' => 'do_not_allow',
				),
				'map_meta_cap'       => true,
				'menu_icon'          => TWITCH_VIDEO_POST_TYPE_URL . '/icons/brand-twitch.svg',
			)
		);
	}

	public static function extra_user_profile_fields( WP_User $user ) {
		if ( in_array( 'author', $user->roles, true ) ) {
			?>
		<h3><?php echo esc_html( TWITCH_BRAND_NAME ); ?></h3>

		<table class="form-table">
			<tr>
				<th><label for="<?php echo esc_attr( TWITCH_USER_FIELD ); ?>"><?php esc_html_e( 'Twitch username', 'twitch-video-post-type' ); ?></label></th>
				<td>
					<input type="text" name="<?php echo esc_attr( TWITCH_USER_FIELD ); ?>" id="<?php echo esc_attr( TWITCH_USER_FIELD ); ?>" value="<?php echo esc_attr( get_the_author_meta( TWITCH_USER_FIELD, $user->data->ID ) ); ?>" class="regular-text"  pattern="[^()/><\][\\\x22,;|]+" />
					<div class="author-social-networks-input-explanation">
						<p><?php esc_html_e( 'Please, write a valid Twitch username.', 'clapclap' ); ?></p>
					</div>
				</td>
			</tr>
		</table>
			<?php
		}
	}

	public static function is_valid_username( string $username ) : bool {
		return ! str_contains( $username, '/' );
	}

	public static function save_extra_user_profile_fields( int $user_id ) {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user_id ) ) {
			return;
		}

		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		$user = get_user_by( 'ID', $user_id );

		if ( in_array( 'author', $user->roles, true ) && isset( $_POST[ TWITCH_USER_FIELD ] ) ) {
			$username = sanitize_text_field( wp_unslash( $_POST[ TWITCH_USER_FIELD ] ) );
			if ( self::is_valid_username( $username ) ) {
				update_user_meta( $user_id, TWITCH_USER_FIELD, $username );
			}
		}
	}

	public static function validate_extra_user_profile_fields( \WP_Error $errors, bool $update, stdClass $user ): void {
		$nonce = empty( $_POST['_wpnonce'] ) ? '' : sanitize_key( wp_unslash( $_POST['_wpnonce'] ) );
		if ( ! $nonce || ! wp_verify_nonce( $nonce, 'update-user_' . $user->ID ) ) {
			return;
		}

		if ( 'author' === $user->role && isset( $_POST[ TWITCH_USER_FIELD ] ) ) {
			$username = sanitize_text_field( wp_unslash( $_POST[ TWITCH_USER_FIELD ] ) );
			if ( ! self::is_valid_username( $username ) ) {
				$errors->add( 'twitch_video_post_type', esc_html__( 'The Twitch username you introduced is not correct.', 'twitch-video-post-type' ) );
			}
		}
	}

	public static function admin_menu() {
		$menu_handle = add_submenu_page( 'edit.php?post_type=' . TWITCH_VIDEO_POST_TYPE, esc_html__( 'Import', 'twitch-video-post-type' ), esc_html__( 'Import', 'twitch-video-post-type' ), 'publish_posts', 'import', array( __CLASS__, 'add_import_page' ) );
		$menu_handle = add_submenu_page( null, esc_html__( 'Imported video', 'twitch-video-post-type' ), esc_html__( 'Imported video', 'twitch-video-post-type' ), 'publish_posts', 'twitch_new_video', array( __CLASS__, 'add_video_import_page' ) );
	}

	public static function hide_admin_menu_item() {
		echo '<style>#menu-posts-twitch-video{display:none}</style>';
	}

	public static function add_import_page() {
		$accepted_conditions = get_user_meta( get_current_user_id(), 'accepted_terms_and_conditions' );
		if ( ! current_user_can( 'administrator' ) && ( ! $accepted_conditions || ! $accepted_conditions[0] ) ) {
			echo '<p><a href="./profile.php">' . esc_html__( 'You need to accept the privacy policy.', 'twitch-video-post-type' ) . '</a></p>';
			return;
		}
		echo '<p>' . esc_html__( 'Streams are deleted automatically by Twitch after a few days. At that point, it will be removed from ClapClap. If you want your content to stay forever, please upload it to YouTube and import it from there.', 'twitch-video-post-type' ) . '</p>';
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Import Twitch videos', 'twitch-video-post-type' ); ?></h1>
			<form action="edit.php?post_type=<?php echo esc_attr( TWITCH_VIDEO_POST_TYPE ); ?>&page=twitch_new_video" method="post">
				<label for="video-id"><?php esc_html_e( 'Twitch stream URL:', 'twitch-video-post-type' ); ?></label><br>
				<input id="video-id" type="url" name="video-id" placeholder="ex: https://www.twitch.tv/videos/1050980797" required style="min-width: 35%;" />
				<?php wp_nonce_field(); ?>
				<div class="submit"><button class="button button-primary" name="add_via_id"><?php esc_html_e( 'Import stream', 'twitch-video-post-type' ); ?></button></div>
			</form>
		</div>
		<?php
	}

	public static function is_twitch_url( array $url ): bool {
		if ( 'https' !== $url['scheme'] && 'http' !== $url['scheme'] ) {
			return false;
		}
		if (
			'www.twitch.tv' !== $url['host'] &&
			'twitch.tv' !== $url['host']
		) {
			return false;
		}

		return true;
	}

	public static function parse_video_id_from_url( string $url ): string {
		$parsed_url = wp_parse_url( trim( $url ) );

		if ( ! self::is_twitch_url( $parsed_url ) ) {
			return '';
		}

		if ( '/videos/' === substr( $parsed_url['path'], 0, 8 ) ) {
			return substr( $parsed_url['path'], 8 );
		}

		if ( str_contains( $parsed_url['path'], '/v/' ) ) {
			$parts = explode( '/v/', $parsed_url['path'] );
			return $parts[1];
		}

		return '';
	}

	public static function add_video_import_page() {
		if ( isset( $_POST['video-id'] ) && is_string( $_POST['video-id'] ) && '' !== $_POST['video-id'] ) {
			check_admin_referer();
			$id = self::parse_video_id_from_url( sanitize_text_field( wp_unslash( $_POST['video-id'] ) ) );
		}

		if ( ! $id ) {
			echo '<div class="notice notice-error"><p>' . esc_html__( 'Video not found. Verify it follows the format https://www.twitch.tv/videos/105098079', 'twitch-video-post-type' ) . '</p></div>';
			return;
		}

		self::print_debug_info( esc_html__( 'Video id:', 'twitch-video-post-type' ) . $id );
		$access_token = Twitch_Utils::get_twitch_access_token();

		$video_response = wp_remote_get(
			'https://api.twitch.tv/helix/videos?id=' . $id,
			array(
				'headers' => array(
					'Client-Id'     => TWITCH_CLIENT_ID,
					'Authorization' => 'Bearer ' . $access_token,
				),
			)
		);

		self::print_debug_info( esc_html__( 'Response:', 'twitch-video-post-type' ), print_r( $video_response, true ) );
		$video = json_decode( $video_response['body'] )->data[0];

		self::print_debug_info( esc_html__( 'Video data:', 'twitch-video-post-type' ), print_r( $video, true ) );
		self::insert_post_from_video( $video );
	}

	private static function get_author_id_from_twitch_username( string $twitch_username ): int {
		$args  = array(
			'role'       => 'author',
			'meta_key'   => TWITCH_USER_FIELD,
			'meta_value' => $twitch_username,
		);
		$users = get_users( $args );

		if ( count( $users ) > 0 ) {
			return $users[0]->ID;
		}

		return 0;
	}

	private static function convert_duration_to_seconds( string $duration ): string {
		if ( strpos( $duration, 'h' ) ) {
			$date_values = date_parse_from_format( 'H\hi\ms\s', $duration );
		} else {
			$date_values = date_parse_from_format( 'i\ms\s', $duration );
		}
		return $date_values['hour'] * 3600 + $date_values['minute'] * 60 + $date_values['second'];
	}

	private static function insert_post_from_video( $video ) {
		$author_id = self::get_author_id_from_twitch_username( $video->user_name );
		if ( ! $author_id ) {
			echo '<div class="notice notice-error"><p>' . esc_html__( 'You need to specify the author:', 'twitch-video-post-type' ) . '</p></div>';
			return;
		}
		if ( ! current_user_can( 'administrator' ) && get_current_user_id() !== $author_id ) {
			echo '<div class="notice notice-error"><p>' . esc_html__( 'You can only import videos from your channel.', 'twitch-video-post-type' ) . '</p></div>';
			return;
		}
		$id            = $video->id;
		$title         = $video->title;
		$description   = $video->description;
		$duration      = $video->duration;
		$published_at  = $video->published_at;
		$thumbnail_url = $video->thumbnail_url;

		$old_video = new WP_Query(
			array(
				'meta_key'   => 'youtube_id',
				'meta_value' => $id,
				'post_type'  => TWITCH_VIDEO_POST_TYPE,
			)
		);

		$post_id = 0;

		if ( count( $old_video->posts ) > 0 ) {
			$post_id = $old_video->posts[0]->ID;
		}

		$new_post_id = wp_insert_post(
			array(
				'ID'           => $post_id,
				'post_author'  => $author_id,
				'post_title'   => $title,
				'post_content' => $description,
				'post_date'    => $published_at,
				'post_status'  => 'publish',
				'post_type'    => TWITCH_VIDEO_POST_TYPE,
				'meta_input'   => array(
					'youtube_thumbnail' => $thumbnail_url,
					'youtube_id'        => $id,
					'duration'          => self::convert_duration_to_seconds( $duration ),
				),
			)
		);

		if ( $post_id !== $new_post_id ) {
			echo '<div class="notice notice-success"><p>' . esc_html__( 'Video imported correctly.', 'twitch-video-post-type' ) . ' <a href="' . esc_url( get_permalink( $new_post_id ) ) . '">' . esc_html__( 'Open', 'twitch-video-post-type' ) . '</a>.</p></div>';
		} else {
			echo '<div class="notice notice-success"><p>' . esc_html__( 'Video updated correctly.', 'twitch-video-post-type' ) . ' <a href="' . esc_url( get_permalink( $new_post_id ) ) . '">' . esc_html__( 'Open', 'twitch-video-post-type' ) . '</a>.</p></div>';
		}
	}

	public static function gutenberg_can_edit_post_type( bool $can_edit, string $post_type ): bool {
		return TWITCH_VIDEO_POST_TYPE === $post_type ? false : $can_edit;
	}

	private static function print_debug_info( string $content, string $json = '' ) {
		if ( ! current_user_can( 'administrator' ) && ( ! defined( WP_DEBUG ) || ! WP_DEBUG ) ) {
			return;
		}
		echo '<div class="notice notice-info">';
		echo '<p>' . esc_html( $content ) . '</p>';
		if ( $json ) {
			echo '<details><summary>' . esc_html__( 'Details', 'twitch-video-post-type' ) . '</summary>';
			echo '<pre>' . esc_html( $json ) . '</pre>';
			echo '</details>';
		}
		echo '</div>';
	}
}

$twitch_post_type = new Twitch_Post_Type();
add_action( 'init', array( $twitch_post_type, 'init' ) );
